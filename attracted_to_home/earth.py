from panda3d.core import CollisionSphere, CollisionNode, NodePath
from panda3d.physics import ActorNode, ForceNode, LinearVectorForce


class Earth:
    def __init__(self, game, model, texture, position, radius):
        self.physics_node = NodePath('planet_physics_node')
        self.physics_node.reparentTo(game.render)

        actor_node = ActorNode('planet_actor_node')
        self.actor_node_path = self.physics_node.attachNewNode(actor_node)
        game.physicsMgr.attachPhysicalNode(actor_node)
        self.planet = game.loader.loadModel(model)
        self.texture = game.loader.loadTexture(texture)
        self.planet.setTexture(self.texture, 1)
        self.planet.reparentTo(self.actor_node_path)
        self.planet.setScale(radius)

        self.gravity = self.planet.attachNewNode('planet_gravity_node')
        self.actor_node_path.setPos(position)

        self.gravity_force_node = ForceNode('planet_gravity_force')
        self.gravity.attachNewNode(self.gravity_force_node)
        self.gravity_force = LinearVectorForce(0, 5, 0)
        self.gravity_force_node.addForce(self.gravity_force)

        # Collision sphere for the Planet
        collision_solid_inner = CollisionSphere(0, radius)
        inner_collision_node_path = self.actor_node_path.attachNewNode(CollisionNode('earth_inner_collision_node'))
        inner_collision_node_path.node().addSolid(collision_solid_inner)

        # Gravity field
        collision_solid_outer = CollisionSphere(0, radius * 3)
        outer_collision_node_path = self.actor_node_path.attachNewNode(CollisionNode('planet_outer_collision_node'))
        outer_collision_node_path.node().addSolid(collision_solid_outer)

        # inner_collision_node_path.show()
        outer_collision_node_path.show()

    def rotate_gravity_node(self, angle):
        self.gravity.setH(angle + 90)
