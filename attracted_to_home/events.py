from direct.showbase import DirectObject


class Events(DirectObject.DirectObject):

    def __init__(self, game):
        super().__init__()
        self.game = game
        self.spaceship = game.spaceship
        self.planets = game.planets
        self.accept('mouse1', self.thrust)
        self.accept('mouse1-up', self.brake)
        self.accept('escape', game.do_exit)
        self.accept('f1', game.toggleWireframe)
        self.accept('f2', game.toggleTexture)
        self.accept('f3', game.toggle_debug)
        self.accept('into_planet_outer_collision_node', self.planet_start_attract)
        self.accept('outof_planet_outer_collision_node', self.planet_stop_attract)
        self.accept('into_planet_inner_collision_node', self.lose_game)
        self.accept('into_earth_inner_collision_node', self.win_game)

    def thrust(self):
        self.spaceship.thrust()

    def brake(self):
        self.spaceship.brake()

    def planet_start_attract(self, entry):
        planet_force = entry.getIntoNodePath().getParent().find("**/planet_gravity_force").node().getForce(0)
        self.spaceship.actor_node_path.node().getPhysical(0).addLinearForce(planet_force)
        self.game.gravity_sound.play()

    def planet_stop_attract(self, entry):
        planet_force = entry.getIntoNodePath().getParent().find("**/planet_gravity_force").node().getForce(0)
        self.spaceship.actor_node_path.node().getPhysical(0).removeLinearForce(planet_force)

    def lose_game(self, entry):
        self.game.lose_game()

    def win_game(self, entry):
        self.game.win_game()
