from panda3d.core import NodePath, CollisionNode, CollisionSphere, CollisionTraverser
from panda3d.physics import ForceNode, LinearVectorForce, ActorNode


class Spaceship:
    def __init__(self, game, model, position, scale):
        self.node = NodePath('spaceship_physics_node')
        self.node.reparentTo(game.render)
        self.game = game

        actor_node = ActorNode('spaceship_actor_node')
        self.actor_node_path = self.node.attachNewNode(actor_node)
        game.physicsMgr.attachPhysicalNode(actor_node)
        self.spaceship = game.loader.loadModel(model)
        self.spaceship.reparentTo(self.actor_node_path)
        self.spaceship.setScale(scale)
        actor_node.getPhysicsObject().setMass(5)

        self.thruster = NodePath('spaceship_thruster_node')
        self.thruster.reparentTo(self.spaceship)
        self.actor_node_path.setPos(position)

        self.thruster_force_node = ForceNode('spaceship_thruster_force_node')
        self.thruster.attachNewNode(self.thruster_force_node)
        self.thruster_force = LinearVectorForce(0, -100, 0)
        self.thruster_force.setMassDependent(1)
        self.thruster_force_node.addForce(self.thruster_force)

        collision_solid_spaceship = CollisionSphere(position, 30)
        self.collision_node_path = self.actor_node_path.attachNewNode(CollisionNode('spaceship_collision_node'))
        self.collision_node_path.node().addSolid(collision_solid_spaceship)

        game.cTrav = CollisionTraverser()

        # self.collision_node_path.show()

    def thrust(self):
        self.actor_node_path.node().getPhysical(0).addLinearForce(self.thruster_force)
        self.game.thruster_sound.play()

    def brake(self):
        self.actor_node_path.node().getPhysical(0).removeLinearForce(self.thruster_force)
        self.game.thruster_sound.stop()

    def rotate(self, angle):
        self.spaceship.setH(angle + 90)
