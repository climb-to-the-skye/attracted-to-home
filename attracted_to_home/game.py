import math
import sys

from direct.gui.DirectDialog import *
from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from panda3d.core import loadPrcFile, Point3, Point2, CollisionHandlerEvent, CollisionTraverser

from attracted_to_home.earth import Earth
from attracted_to_home.planet import Planet
from attracted_to_home.spaceship import Spaceship
from attracted_to_home.events import Events


class Game(ShowBase):
    def __init__(self):
        loadPrcFile("attracted_to_home/etc/Config.prc")
        super().__init__()
        self.debug_enabled = False

        # Sounds and music
        main_music = self.loader.loadSfx("sfx/music.ogg")
        main_music.setLoop(True)
        main_music.play()
        self.thruster_sound = self.loader.loadSfx("sfx/rocket_thrusters.ogg")
        self.thruster_sound.setLoop(True)
        self.gravity_sound = self.loader.loadSfx("sfx/attract.ogg")
        self.explosion_sound = self.loader.loadSfx("sfx/explosion.ogg")

        # Background texture
        self.sky = self.loader.loadModel("models/solar_sky_sphere")

        self.sky_tex = self.loader.loadTexture("models/tex/space.jpg")
        self.sky.setTexture(self.sky_tex, 1)
        self.sky.reparentTo(self.render)
        self.sky.setScale(1000)

        self.setBackgroundColor(0, 0, 0)
        self.enableParticles()
        self.cTrav = CollisionTraverser()

        # Drawn objects
        self.planets = None
        self.spaceship = None
        self.setup_planets_spaceship()

        # Collisions handler
        self.collision_handler = CollisionHandlerEvent()
        self.collision_handler.addInPattern('into_%in')
        self.collision_handler.addOutPattern('outof_%in')
        self.cTrav.addCollider(self.spaceship.collision_node_path, self.collision_handler)

        # Events handler
        self.events = Events(self)
        self.taskMgr.add(self.routines, 'Routines')

        # Camera setup
        self.camera.setPos(0, 0, 2000)  # Set the camera position (X, Y, Z)
        self.camera.setHpr(0, -90, 0)  # Set the camera orientation (heading, pitch, roll) in degrees

        # Input
        self.disableMouse()
        self.replay_quit_dialog = None

    def reset_planets_spaceship(self):
        self.setup_planets_spaceship()
        self.cTrav.addCollider(self.spaceship.collision_node_path, self.collision_handler)
        self.events.spaceship = self.spaceship
        self.events.planets = self.planets

    def remove_planets_spaceship(self):
        self.spaceship.node.removeNode()
        for planet in self.planets:
            planet.physics_node.removeNode()

    def setup_planets_spaceship(self):
        self.planets = [
            Earth(self, 'models/sphere', 'models/tex/Earth-hires.jpg', (500, 400, 0), 50),
            Planet(self, 'models/sphere', 'models/mercury.jpg', (150, 100, 0), 50),
            Planet(self, 'models/sphere', 'models/mercury.jpg', (-150, -100, 0), 50),
        ]
        self.spaceship = Spaceship(self, 'models/spaceship', (0, 0, 0), 20)

    def get_mouse_absolute_coordinates(self):
        return self.convert_normalized_2d_to_absolute(self.get_mouse_normalized_coordinates())

    def get_mouse_normalized_coordinates(self):
        if self.mouseWatcherNode.hasMouse():
            return self.mouseWatcherNode.getMouseX(), self.mouseWatcherNode.getMouseY()
        else:
            return 0, 0

    def convert_normalized_2d_to_absolute(self, normalized_coordinates=(0, 0)):
        window_size = self.win.getSize()
        if normalized_coordinates is None:
            normalized_coordinates = (0, 0)

        return normalized_coordinates[0] * window_size[0], normalized_coordinates[1] * window_size[1]

    def compute_absolute_2d_position(self, node_path, point=Point3(0, 0, 0)):
        normalized_coordinates = self.compute_normalized_2d_position(node_path, point)
        return self.convert_normalized_2d_to_absolute(normalized_coordinates)

    def compute_normalized_2d_position(self, node_path, point=Point3(0, 0, 0)):
        """ Computes a 3-d point, relative to the indicated node, into a
        2-d point as seen by the camera.  The range of the returned value
        is based on the len's current film size and film offset, which is
        (-1 .. 1) by default. """

        # Convert the point into the camera's coordinate space
        p3d = self.cam.getRelativePoint(node_path, point)

        # Ask the lens to project the 3-d point to 2-d.
        p2d = Point2()
        if self.camLens.project(p3d, p2d):
            return p2d

        # If project() returns false, it means the point was behind the lens.
        return None

    @staticmethod
    def compute_rotation_angle(point1_coordinates, point2_coordinates):
        angle_rad = math.atan2(point1_coordinates[1] - point2_coordinates[1], point1_coordinates[0] - point2_coordinates[0])
        angle_deg = math.degrees(angle_rad)
        return angle_deg

    def routines(self, task):
        ship_absolute_coordinates = self.compute_absolute_2d_position(self.spaceship.actor_node_path)
        mouse_absolute_coordinates = self.get_mouse_absolute_coordinates()
        self.spaceship.rotate(self.compute_rotation_angle(mouse_absolute_coordinates, ship_absolute_coordinates))
        for planet in self.planets:
            planet_absolute_position = self.compute_absolute_2d_position(planet.actor_node_path)
            rotation_angle = self.compute_rotation_angle(ship_absolute_coordinates, planet_absolute_position)
            planet.rotate_gravity_node(rotation_angle)
        return Task.cont

    def do_exit(self):
        # self.cleanup()
        sys.exit(1)

    def toggle_debug(self):
        self.debug_enabled = not self.debug_enabled
        self.setFrameRateMeter(self.debug_enabled)

    def lose_game(self):
        def item_selection(arg):
            if arg:
                self.do_exit()
            else:
                self.reset_game()

        self.explosion_sound.play()
        self.remove_planets_spaceship()
        self.replay_quit_dialog = DirectDialog(dialogName="replay_quit_dialog", text='You lost... :\'(',
                                               buttonTextList=('Play again :D', 'Give up >:('),
                                               command=item_selection, fadeScreen=1)

    def win_game(self):
        def item_selection(arg):
            if arg:
                self.do_exit()
            else:
                self.reset_game()

        self.remove_planets_spaceship()
        self.replay_quit_dialog = DirectDialog(dialogName="replay_quit_dialog", text='You won ! EZ PZ Lemon Squeezie',
                                               buttonTextList=('Play again :D', 'Quit on a win ;)'),
                                               command=item_selection, fadeScreen=1)

    def reset_game(self):
        self.reset_planets_spaceship()
        self.replay_quit_dialog.cleanup()
