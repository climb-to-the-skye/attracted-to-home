Attracted to Home
=================

A game created for the Global Game Jam 2019. It is written in Python, using the Panda3D engine.

Dependencies
------------
* Python >= 3.7
* Panda3D

Licence
-------
This project is licenced under CC-BY-NC-SA 3.0

Authors
-------
* Mayeul Lemennicier
* Pierre Gillet
